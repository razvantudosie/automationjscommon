'use strict';

var client = require("pkgcloud").storage.createClient({
    provider: "rackspace",
    username: process.env.RACKSPACE_USER,
    apiKey: process.env.RACKSPACE_KEY,
    region: process.env.RACKPSACE_REGION
});

function getOrCreateContainer(name, callback) {
    client.getContainer(name, function gotContainer(err, container) {
        if (!err) {
            return callback(null, container);
        }
        else {
            return client.createContainer({ name: name }, callback);
        }
    });
}

function getContainer(name, callback) {
    client.getContainer(name, function gotContainer(err, container) {
        if (err) {
            return callback(err);
        }
        callback(null, container);
    });
}

function writeRawFile(container, id, data, callback) {
    getOrCreateContainer(container, function gotOrCreatedContainer(err, container) {

        if (err) {
            return callback(err);
        }

        var writeStream = client.upload({
            container: container,
            remote: id
        });

        writeStream.on("error", function writeError(err) {
            return callback(err, null);
        });

        writeStream.on("success", function writeSuccess() {
            return callback(null);
        });

        writeStream.end(new Buffer(data, "ascii"));
    });
}

module.exports = {
    writeRawFile: writeRawFile
};
