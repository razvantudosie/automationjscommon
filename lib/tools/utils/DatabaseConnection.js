var mysql = require('mysql');
var mongodb = require('mongodb').MongoClient;
//var oracle = require('oracledb');

function connectToMySQL() {
    return mysql.createConnection({
        host: process.env.MYSQL_HOST,
        user: process.env.MYSQL_USERNAME,
        port: process.env.MYSQL_PORT,
        password: process.env.MYSQL_PASSWORD
    });
}

function disconnectFromMySQL(connection) {
    connection.end();
}

function connectToMongo(db, cb) {
    var auth = process.env.MONGO_DB_USERNAME ? process.env.MONGO_DB_USERNAME + ":" +
        process.env.MONGO_DB_PASSWORD + "@" : "";

    var mongourl = "mongodb://" +
        auth +
        process.env.MONGO_DB_HOST + ":" +
        process.env.MONGO_DB_PORT + "/" + db;

    mongodb.connect(mongourl, function (err, db) {
        if (err) {
            console.log(err);
            cb(err, null)
        }
        else {
            cb(null, db);
        }
    });
}

function disconnectFromMongo(connection) {
    connection.close();
}

function connectToOracle(cb) {
    oracle.getConnection(
        {
            user: process.env.ORACLE_DB_USER,
            password: process.env.ORACLE_DB_PASSWORD,
            connectString: process.env.ORACLE_DB_CONNECTION
        },
        function (err, db) {
            if (err) {
                console.log("Connection Error: " + err);
                cb(err, null);
            }
            else {
                cb(null, db);
            }
        });
}

function disconnectFromOracle(connection) {
    connection.close(
        function (err) {
            if (err) {
                console.log(err);
            }
        });
}

module.exports = {
    connectToMySQL: connectToMySQL,
    disconnectMySQL: disconnectFromMySQL,
    connectToMongo: connectToMongo,
    disconnectMongo: disconnectFromMongo,
    connectToOracle: connectToOracle,
    disconnectOracle: disconnectFromOracle
}