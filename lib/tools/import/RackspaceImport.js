var rackspace = require("../utils/RackspaceUtils.js");

var driverPhotoContainer = process.env.RACKSPACE_DRIVER_PHOTOGRAPH_CONTAINER;

function uploadFile(id, data, container, cb){
    container = container || driverPhotoContainer;
    rackspace.writeRawFile(container, id, data, cb);
}

module.exports = {
    uploadFile: uploadFile
};
