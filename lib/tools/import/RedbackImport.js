var datasourceUtils = require('../utils/DatasourceUtils');
var fs = require('fs');
var request = require('request');
var _ = require('lodash');

// TODO: Automatically derive the name
function fromFile(name, file, cb) {
    var file = datasourceUtils.readFile(file);

    var redbackFile = [];

    var sheetNames = _.keys(file.file.Sheets);
    for (var i = 0; i < sheetNames.length; i++) {

        var sheetName = sheetNames[i];
        var sheetArray = file.sheetToArray(file.file.Sheets[sheetName]);

        sheetArray.forEach(function(row) {
            var redbackObject = {
                inputFields: [],
                outputFields: [],
                objectName: "",
                methodName: "",
                description: ""
            };

            redbackObject.objectName = row.objectName;
            redbackObject.methodName = row.methodName;
            redbackObject.description = row.description;

            for(col in row) {
                if(col.startsWith("i_")) {
                    redbackObject.inputFields.push({
                        fieldName: col.replace("i_", ""),
                        fieldVal: row[col]
                    });
                } else if(col.startsWith("o_")) {
                    redbackObject.outputFields.push({
                        fieldName: col.replace("o_", ""),
                        fieldVal: row[col]
                    });
                }
            }

            redbackFile.push(redbackObject);
        });
    }

    upload(name, "redbackTestData", redbackFile, cb);
}

function fromRawFile(name, file, cb) {
    fs.readFile(file, 'utf8', function(err, data) {
        if(err) {
            return cb(err);
        }

        upload(name, "redbackTestData", data, cb)
    });
}

// TODO: place it in utils
function upload(name, type, stub, cb) {
    var username = process.env.IMPORT_API_USERNAME;
    var password = process.env.IMPORT_API_PASSWORD;
    var url = process.env.IMPORT_API_URL;

    var body = {
        name: name,
        type: type,
        stub: JSON.stringify(stub)
    }

    request({
        uri: url,
        headers: {
            "Content-Type": "application/json"
        },
        auth: {
            'user': username,
            'pass': password,
            'sendImmediately': true
        },
        method: 'POST',
        body: JSON.stringify(body)
    }, cb);
}

module.exports = {
    fromFile: fromFile,
    fromRawFile: fromRawFile
}