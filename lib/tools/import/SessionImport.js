var mysql = require('mysql');
var formatters = require('../utils/Formatters');
var fs = require('fs');
var path = require('path');

var excelFile = require("xlsx");
var async = require("async");

function sessionImport(callback) {

    // Read SessionData:
    var sessionData = JSON.parse(fs.readFileSync(path.join(__dirname,"../../resource/SessionData.json"), 'utf8'));

    var connection = databaseConnection.connectToMySQL();

    // Deleting data in session tables based on the key
    // of each object in SessionData.json.
    var deleteSessionData = function (next) {
        async.each(sessionData, function (data, cb) {
            deleteData(connection, Object.keys(data)[0], cb);
        }, function (err) {
            if (err) {
                //console.log(err);
                next(err);
            }
            else {
                //console.error("Session Tables Cleared.");
                next(null);
            }
        });
    }

    //Get the column names and types
    var getColumnData = function (next) {

        var columnTypesArray = {};
        async.each(sessionData, function (data, cb) {

            //Get the table schema and name
            var table = Object.keys(data)[0];
            var tableSchema = table.split(".")[0];
            var tableName = table.split(".")[1];

            var sql = "SELECT COLUMN_NAME, DATA_TYPE FROM information_schema.COLUMNS where TABLE_SCHEMA = '" + tableSchema + "' and TABLE_NAME = '" + tableName + "'";

            connection.query(sql, function (err, columnType) {
                if (err) {
                    //console.log(err);
                   cb(err);
                }
                var columnData = {};

                //retrieves the column name and type for each table
                for (var i = 0; i < columnType.length; i++) {
                    var key = columnType[i].COLUMN_NAME;
                    var value = columnType[i].DATA_TYPE;
                    columnData[key] = value;
                }
                //var tableData={};
                //tableData[table] = columnData;
                columnTypesArray[table] = columnData;
                cb();
            });
        }, function (err) {
            if (err) {
               //console.log(err);
                next(err);
            } else {
                next(null, columnTypesArray)
            }
        })
    }

    //Insert Table data using the column data in the above function
    var insertSessionData = function (columnData, next) {
        async.each(sessionData, function (data, tableCB) {
            var table = Object.keys(data)[0];
            var tableData = data[table];
            var columns = tableData.splice(0, 1)[0];

            async.each(tableData, function (row, rowCB) {
                insert(connection, table, row, columns, columnData[table], rowCB);
            }, function (err) {
                if (err) {
                    //console.log(err);
                   tableCB(err);
                }
                else {
                    tableCB();
                }
            })

        }, function (err) {
            if (err) {
               //console.log(err);
                next(err);
            } else {
                //console.error("Session Data Inserted.");
                next(null);
            }
        });
    }

    async.waterfall([deleteSessionData, getColumnData, insertSessionData], function (err, results) {

        databaseConnection.disconnectMySQL(connection);
        if (err) {
           //console.log(err);
            callback(err);
        }
        else {
            console.log("SessionImport ... done");
            callback();
        }
    });
}

function insert(connection, table, row, columns, columnTypes, cb) {
    row = row.map(function (cell, index) {
        var column = columns[index];
        var columnType = columnTypes[column];
        return formatters.format(String(cell), columnType);
    });
    var sql = "INSERT INTO " + table + " VALUES (" + row.join(",") + ")";
    connection.query(sql, function (err, response) {
        if (err) {
            cb(err);
        }
        else {
            cb();
        }
    });
}

function deleteData(connection, table, cb) {
    var sql = "DELETE FROM " + table;
    connection.query(sql, function (err, response) {
        if (err) {
            cb(err);
        }
        else {
            cb();
        }
    });
}

module.exports = sessionImport;
